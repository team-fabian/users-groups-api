import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Member from "App/Models/Member";
export default class MembersController {
  public async store({ request }: HttpContextContract) {
    //const membersData = request.only(["group_id", "user_id"]);

    const member = new Member();

    member.groupId = request.input("group_id");

    member.userId = request.input("user_id");

    // member.merge(membersData);

    await member.save();

    return member;
  }

  public async index({ response }: HttpContextContract) {
    const membersData = await Member.all();

    return response.json({ membersData });
  }

  public async getMembers({ params, response }) {
    try {
      const gropumembers = await Member.query().where(
        "group_id",
        params.groupID
      );

      if (gropumembers) {
        return response.json({ gropumembers });
      }
    } catch (error) {
      console.log(error);
    }

    return "No data found";
  }
}
