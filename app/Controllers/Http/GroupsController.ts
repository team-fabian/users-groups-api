import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Group from "App/Models/Group";

export default class GroupsController {
  public async store({ request }: HttpContextContract) {
    const groupData = request.only([
      "user_id",
      "title",
      "founder",
      "topics",
      "desc",
    ]);

    const group = new Group();

    await group.merge(groupData).save();

    return group;
  }

  public async index({ response }: HttpContextContract) {
    const groups = await Group.all();

    return response.json({ groups });
  }

  public async find({ response, params }) {
    try {
      const group = await Group.find(params.id);

      if (group) {
        return response.json({ group });
      }
    } catch (error) {
      console.log(error);
    }
  }

  public async delete({ params }) {
    try {
      const group = await Group.find(params.id);

      if (group) {
        await group.delete();

        return "group Deleted Successfully";
      }
    } catch (error) {
      console.log(error);
    }

    return "group does not exist on servers ";
  }
}
