import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

import User from "App/Models/User";

export default class UsersController {
  public async store({ request }: HttpContextContract) {
    const data = request.only(["name", "email", "password", "location"]);
    const user = new User();

    // user.name = "Gautam";

    // user.email = "gautam@mb.com";

    // user.password = "pass2k23";

    // user.location = "mumbai";

    // another method  using merge method

    // user.merge({
    //   name: "mak",

    //   email: "mak@mkm.xom",

    //   password: "password123",

    //   location: "canada",
    // });

    await user.merge(data).save();

    //   await user.save();    //

    return user;
  }

  public async index({ response }: HttpContextContract) {
    // const users = await User.findMany([1,2,3]);    to run more number of queries at once

    //  const users = await User.findBy('name',"mak");  To find wether the property exist in the database

    const users = await User.all();

    return response.json({ users });
  }

  public async find({ response, params }: HttpContextContract) {
    try {
      const user = await User.find(params.id);

      if (user) {
        return response.json({ user });
      }
    } catch (error) {
      console.log(error);
    }
  }

  public async delete({ params }: HttpContextContract) {
    try {
      const user = await User.find(params.id);

      if (user) {
        await user.delete();
        return "user Deleted Successfully";
      }
    } catch (error) {
      console.log(error);
    }
    return "user not found on servers";
  }
}
