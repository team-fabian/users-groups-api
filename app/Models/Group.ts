import { DateTime } from "luxon";

import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  hasMany,
  HasMany,
} from "@ioc:Adonis/Lucid/Orm";
import User from "./User";
import Member from "./Member";
export default class Group extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({ columnName: "user_id" })
  public userId: number;

  @column()
  public title: string;

  @column()
  public founder: string;

  @column()
  public topics: string;

  @column()
  public desc: string;

  @column.dateTime({ autoCreate: true, columnName: "created_at" })
  public createdAt: DateTime;

  @column.dateTime({
    autoCreate: true,
    autoUpdate: true,
    columnName: "updated_at",
  })
  public updatedAt: DateTime;

  @belongsTo(() => User)
  public owner: BelongsTo<typeof User>;

  @hasMany(() => User)
  public users: HasMany<typeof User>;

  @hasMany(() => Member)
  public member: HasMany<typeof Member>;
}
