import { DateTime } from "luxon";
import {
  BaseModel,
  beforeSave,
  column,
  hasMany,
  HasMany,
} from "@ioc:Adonis/Lucid/Orm";
import Group from "./Group";
import Hash from "@ioc:Adonis/Core/Hash";
export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column()
  public name: string;
  @column()
  public email: string;
  @column({ serializeAs: null })
  public password: string;

  @column()
  public location: string;

  @column()
  public rememberMeToken: string | null;

  @column.dateTime({ autoCreate: true, columnName: "created_at" })
  public createdAt: DateTime;

  @column.dateTime({
    autoCreate: true,
    autoUpdate: true,
    columnName: "updated_at",
  })
  public updatedAt: DateTime;

  @hasMany(() => Group)
  public groups: HasMany<typeof Group>;

  @hasMany(() => Group)
  public ownGroups: HasMany<typeof Group>;

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password);
    }
  }
}
